package stuff;

import java.awt.Color;
import java.awt.Graphics;

public class HUD {

	public static float HEALTH = 100;
	
	private int score = 0;
	private int level = 1;
	
	public HUD() {
		
	}

	public void tick() {
		//HEALTH--;
		
		HEALTH= Game.clamp(HEALTH, 0 , 100);
		
		score++;
	}
	
	public void render(Graphics g) {
		g.setColor(Color.RED);
		g.fillRect(15, 15, 200, 32);
		g.setColor(Color.GREEN);
		g.fillRect(15, 15, (int) (HEALTH*2), 32);
		
		g.drawString("Score: " + score, 15, 64);
		g.drawString("Level: " + level, 16, 80);
	}
	
	public void setScore(int score) {
		this.score = score;
	}
	
	public int getScore() {
		return score;
	}
	
	public void setLevel(int level) {
		this.level = level;
	}
	
	public int getLevel() {
		return level;
	}
	
	public void dead() {
		HEALTH = 100;
		score = 0;
		level = 1;
	}
	
}
