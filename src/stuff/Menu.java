package stuff;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

import stuff.Game.STATE;

public class Menu extends MouseAdapter {

	private Game game;
	private Handler handler;
	private HUD hud;
	private Random r = new Random();
	
	public Menu(Game game, Handler handler, HUD hud
			) {
		this.game = game;
		this.handler = handler;
		this.hud = hud;
	}

	public void mousePressed(MouseEvent e) {
		int mx = e.getX();
		int my = e.getY();
		
		if (game.gameState == STATE.Menu) {
			//Play Button
			if(mouseOver(mx, my, 270, 240, 200, 64)) {
				game.gameState = STATE.Game;
				handler.clearEnemies();
				handler.addObject(new Player(Game.WIDTH/2, Game.HEIGHT/2, ID.Player, handler));
				handler.addObject(new Enemy(Game.WIDTH/2 + 40, Game.HEIGHT/2, ID.Enemy, handler));
				
			}
			
			//Quit Button
			if (mouseOver(mx,my, 270, 490, 200, 64)) {
				System.exit(1);;
			}
		}
		
		else if (game.gameState == STATE.End) {
			if (mouseOver(mx,my,270,495,200,64)) {
				game.gameState = STATE.Menu;
				for (int i=0; i<15;i++) {
					handler.addObject(new MenuParticle(r.nextInt(Game.WIDTH), r.nextInt(Game.HEIGHT), ID.MenuParticle, handler));
				}
			}
		}
		
		
		
	}
	
	public void mouseReleased(MouseEvent e) {
		
	}
	
	private boolean mouseOver(int mx, int my, int x, int y, int width, int height) {
		if (mx > x && mx < x+width) {
			if (my > y && my < y + height) {
				return true;
			}else return false;
		} else return false;
	}
	
	public void tick() {
		
	}
	
	public void render(Graphics g) {
		Font fnt = new Font("arial",1,55);
		Font fnt2 = new Font("arial",1,30);
		
		if (game.gameState == STATE.Menu) {
			g.setFont(fnt);
			g.setColor(Color.white);
			g.drawString("Menu", Game.WIDTH/2-70, 140);
			g.setFont(fnt2);
			g.drawString("Play", Game.WIDTH/2-40, Game.HEIGHT/4+48+32+46);
			g.drawString("Yes", Game.WIDTH/2-34, Game.HEIGHT/4+48+32+46 + 130);
			g.drawString("Quit", Game.WIDTH/2-40, Game.HEIGHT/4+48+32+46 + 130 + 126);
			
			
			g.setColor(Color.white);
			g.drawRect(270, 240, 200, 64);
			g.drawRect(270, 370, 200, 64);
			g.drawRect(270, 495, 200, 64);
		}
		else if (game.gameState == STATE.End) {
			g.setFont(fnt);
			g.setColor(Color.white);
			g.drawString("Game Over", 200, 70);
			
			g.setFont(fnt2);
			g.drawString("You died with a Score of: " + hud.getScore(), 150, 150);
			
			
			g.drawString("Main Menu", 294, 540);
			g.setColor(Color.white);
			g.drawRect(270, 495, 200, 64);
		}
		
	}
	
	
}
