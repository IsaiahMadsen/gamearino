package stuff;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;


public class Player extends GameObject {

	private Random r;
	Handler handler;
	
	public Player(int x, int y, ID id, Handler handler) {
		super(x, y, id);
		this.handler = handler;
		
	}
	
	public void tick() {		
		x += spdX;
		y += spdY;
		
		x = Game.clamp(x,0,Game.WIDTH-38);
		y = Game.clamp(y,0,Game.HEIGHT-60);
		
		collision();
	}

	public void render(Graphics g) {
		g.setColor(Color.white);
		g.fillRect((int)x,(int) y, 32, 32);
	}
	
	public Rectangle getBounds() {
		return new Rectangle((int)x,(int)y,32,32);
	}
	
	private void collision() {
		for (int i=0;i<handler.objects.size();i++) {
			GameObject temp = handler.objects.get(i);
			
			if (temp.getID() == ID.Enemy || temp.getID() == ID.SmartEnemy) {
				if (getBounds().intersects(temp.getBounds())) {
					HUD.HEALTH-=2;
				}
			}
			
		}

	}
	
}
