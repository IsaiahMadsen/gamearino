package stuff;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Random;

public class Enemy extends GameObject {

	private Random r = new Random();
	private Handler handler;
	private Color color;
	
	
	
	public Enemy(float x, float y, ID id, Handler handler) {
		super(x, y, id);
		spdY = 5;
		spdX = 5;
		this.handler = handler;
	}

	@Override
	public void tick() {
		x+= spdX;
		y += spdY;

		if (y<= 0 || y>= Game.HEIGHT-32) spdY *= -1;
		if (x<= 0 || x>= Game.WIDTH-16) spdX *= -1;
	
		handler.addObject(new Trail(x,y,ID.Trail,16f,16f,0.09f,Color.RED,handler));
	}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.red);
		g.fillRect((int)x, (int)y, 16, 16);
	}
	
	public Rectangle getBounds() {
		return new Rectangle((int)x,(int)y,16,16);
	}

	
}
