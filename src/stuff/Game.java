package stuff;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.Random;

public class Game extends Canvas implements Runnable{

	public static final int WIDTH = 740, HEIGHT = 640;
	
	private Thread thread;
	private boolean running = false;
	
	private Random r = new Random();
	private Handler handler;
	private HUD hud;
	private Spawner spawner;
	private Menu menu;
	private BufferedImage sprite_sheet;
	
	public enum STATE{
		Menu,
		Game,
		End
	};
	
	public static STATE gameState = STATE.Menu;
	
	public Game() {
		handler = new Handler();
		hud = new HUD();
		spawner = new Spawner(handler, hud);
		menu = new Menu(this, handler, hud);
		this.addKeyListener(new Input(handler));
		this.addMouseListener(menu);
		
		AudioPlayer.load();
		AudioPlayer.getMusic("music").loop(1, .2f);

		new window(WIDTH, HEIGHT, "Let's build a game!", this);
		
		ImageLoader loader = new ImageLoader();
		sprite_sheet = loader.loadImage("/sprite_sheet.png");
		
		
		if (gameState == STATE.Game) {
			
		}
		else if (gameState == STATE.Menu) {
			for (int i=0; i<15;i++) {
				handler.addObject(new MenuParticle(r.nextInt(Game.WIDTH), r.nextInt(Game.HEIGHT), ID.MenuParticle, handler));
			}
		}
		
		
	}
	
	public static float clamp(float var, float min, float max) {
		if (var >= max) {
			return var = max;
		}
		else if (var <= min) {
			return var = min;
		}
		else{
			return var;
		}
	}
	
	public synchronized void start() {
		thread = new Thread(this);
		thread.start();
		running = true;
	}
	
	public synchronized void stop() {
		try {
			thread.join();
			running = false;
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void run() {
		this.requestFocus();
		long lastTime = System.nanoTime();
		double amountOfTicks = 60.0;
		double ns = 1000000000 / amountOfTicks;
		double delta = 0;
		long timer = System.currentTimeMillis();
		int frames = 0;
		while (running) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			while (delta >= 1) {
				tick();
				delta--;
			}
			if (running){
				render();
			}
			frames++;
			
			if (System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
				System.out.println("FPS: " + frames);
				frames = 0;
			}
			
		}
		stop();
	}
	
	private void tick() {
		handler.tick();
		if (gameState == STATE.Game) {
			hud.tick();
			spawner.tick();
			if (hud.HEALTH <= 0) {
				hud.dead();
				gameState = STATE.End;
				handler.clearEnemies();
				//handler.clearPlayers();

			}
		}
		if (gameState == STATE.Menu || gameState == STATE.End) {
			menu.tick();
		}

	}
	
	private void render() {
		BufferStrategy bs = this.getBufferStrategy();
		if (bs == null) {
			this.createBufferStrategy(3);
			return;
		}
		
		Graphics g = bs.getDrawGraphics();
		
		g.setColor(Color.black);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		
		handler.render(g);
		
		if (gameState == STATE.Game) {
			hud.render(g);
		}
		if (gameState == STATE.Menu || gameState == STATE.End) {
			menu.render(g);
		}
		
		
		g.dispose();
		bs.show();
		
	}
	
	public static void main(String args[]) {
		new Game();
	}

}
